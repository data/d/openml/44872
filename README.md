# OpenML dataset: solar_flare

https://www.openml.org/d/44872

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

Predict the number of common solar flares.

The database contains 3 classes, one for the number of times a certain type of solar flare occurred in a 24 hour period.

Each instance represents captured features for 1 active region on the sun.

**Attribute Description**

1. *class* - code for class (modified Zurich class) (A,B,C,D,E,F,H)
2. *largest_spot_size* -  Code for largest spot size (X,R,S,A,H,K)
3. *spot_distribution* - Code for spot distribution (X,O,I,C)
4. *activity* - activity (1 = reduced, 2 = unchanged)
5. *evolution* - evolution (1 = decay, 2 = no growth, 3 = growth)
6. *previous_activity* - previous 24 hour flare activity code (1 = nothing as big as an M1, 2 = one M1, 3 = more activity than one M1)
7. *complex* - historically-complex (1 = Yes, 2 = No)
8. *complex_path* - whether region become historically complex on this pass across the sun's disk (1 = yes, 2 = no)
9. *area* - area (1 = small, 2 = large)
10. *area_largest* - area of the largest spot
11. *c_class_flares* - number of C-class flares production by this region in the following 24 hours (common flares), target feature
12. *m_class_flares* - number of M-class flares production by this region in the following 24 hours (moderate flares)
13. *x_class_flares* - number of X-class flares production by this region in the following 24 hours (severe flares)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44872) of an [OpenML dataset](https://www.openml.org/d/44872). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44872/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44872/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44872/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

